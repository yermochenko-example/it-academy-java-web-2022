public class Main {
	public static void main(String[] args) {
		char a = 198;
		System.out.println(a);
		char b = '\u00C6';
		System.out.println(b);
		char c1 = '\r'; // символ возврата каретки
		char c2 = '\n'; // символ перехода на новую строку
		char c3 = '\t'; // символ табуляции
		char c4 = '\''; // символ одиночной кавычки
		char c5 = '\"'; // символ двойной кавычки
		char c6 = '\\'; // символ обратного слэша
		char x = 'а';
		while(x <= 'я') {
			System.out.println(x);
			x++;
		}
		int n = 'Я' - 'А' + 1;
		System.out.println("n = " + n);
	}
}
