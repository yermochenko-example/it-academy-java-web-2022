public class Queen extends Figure {
	private static final Rook rook = new Rook(null);
	private static final Bishop bishop = new Bishop(null);

	protected Queen(Color color) {
		super(color, "queen");
	}

	@Override
	public boolean canMove(Cell start, Cell finish) {
		return rook.canMove(start, finish) || bishop.canMove(start, finish);
	}
}
