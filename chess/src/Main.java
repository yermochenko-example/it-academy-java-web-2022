import java.util.Scanner;

public class Main {
	public static Cell input(String str) {
		if(str.length() == 2) {
			return new Cell(str.charAt(0), str.charAt(1) - '1' + 1);
		} else {
			return null;
		}
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Введите цвет фигуры (black или white): ");
		String colorName = scanner.nextLine();
		Color color;
		try {
			color = Color.valueOf(colorName.toUpperCase());
		} catch(IllegalArgumentException e) {
			System.out.println("Ошибка: цвет \"" + colorName + "\" неправильный");
			return;
		}
		Figure figure;
		System.out.print("Введите тип фигуры (rook, bishop или queen): ");
		String type = scanner.nextLine();
		if(type.equals("rook")) {
			figure = new Rook(color);
		} else if(type.equals("bishop")) {
			figure = new Bishop(color);
		} else if(type.equals("queen")) {
			figure = new Queen(color);
		} else {
			System.out.println("Неверный тип фигуры");
			return;
		}
		System.out.print("Введите координаты стартовой клетки: ");
		Cell start = input(scanner.nextLine());
		if(start == null) {
			System.out.println("Некорректные координаты");
			return;
		}
		System.out.print("Введите координаты финишной клетки: ");
		Cell finish = input(scanner.nextLine());
		if(finish == null) {
			System.out.println("Некорректные координаты");
			return;
		}
		if(figure.canMove(start, finish)) {
			System.out.println(figure + " может сделать ход " + start + "-" +finish);
		} else {
			System.out.println(figure + " не может сделать ход " + start + "-" +finish);
		}
	}
}
