abstract public class Figure {
	private final Color color;
	private final String name;

	protected Figure(Color color, String name) {
		this.color = color;
		this.name = name;
	}

	public Color getColor() {
		return color;
	}

	@Override
	public String toString() {
		return color.toString().toLowerCase() + " " + name;
	}

	abstract public boolean canMove(Cell start, Cell finish);
}
