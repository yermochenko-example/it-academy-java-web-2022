public class Bishop extends Figure {
	public Bishop(Color color) {
		super(color, "bishop");
	}

	@Override
	public boolean canMove(Cell start, Cell finish) {
		return Cell.dX(start, finish) == Cell.dY(start, finish);
	}
}
