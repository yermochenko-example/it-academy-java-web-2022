public class Rook extends Figure {
	protected Rook(Color color) {
		super(color, "rook");
	}

	@Override
	public boolean canMove(Cell start, Cell finish) {
		return Cell.dX(start, finish) == 0 || Cell.dY(start, finish) == 0;
	}
}
