public class Cell {
	private int x = 1;
	private int y = 1;

	public Cell(char x, int y) {
		if('A' <= x && x <= 'H' && 1 <= y && y <= 8) {
			this.x = x - 'A' + 1;
			this.y = y;
		}
	}

	public char getX() {
		return (char)('A' + x - 1);
	}

	public int getY() {
		return y;
	}

	public String toString() {
		return String.valueOf(getX()) + String.valueOf(getY());
	}

	public static int dX(Cell a, Cell b) {
		return Math.abs(a.x - b.x);
	}

	public static int dY(Cell a, Cell b) {
		return Math.abs(a.y - b.y);
	}
}
