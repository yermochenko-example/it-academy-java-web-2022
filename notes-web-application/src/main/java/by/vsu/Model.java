package by.vsu;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Model {
	private final static Map<Integer, Note> notes = new HashMap<>(Map.ofEntries(
		Map.entry(1, new Note(1, "abc", "test 123", false)),
		Map.entry(2, new Note(2, "xyz", "test 456", true)),
		Map.entry(3, new Note(3, "mnk", "test 789", false)),
		Map.entry(4, new Note(4, "pqr", "test 357", false))
	));

	public static void create(Note note) {
		Integer id = 1;
		if(!notes.isEmpty()) {
			id += Collections.max(notes.keySet());
		}
		note.setId(id);
		notes.put(id, note);
	}

	public static List<Note> readAll() {
		return List.copyOf(notes.values());
	}

	public static void update(Note note) {
		notes.replace(note.getId(), note);
	}

	public static void delete(Integer id) {
		notes.remove(id);
	}
}
