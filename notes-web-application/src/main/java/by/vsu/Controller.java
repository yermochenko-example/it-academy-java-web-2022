package by.vsu;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class Controller extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Note> notes = Model.readAll();
		req.setAttribute("notes", notes);
		req.getRequestDispatcher("/WEB-INF/view.jsp").forward(req, resp);
	}
}
