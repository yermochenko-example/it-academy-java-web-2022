<%@page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
	<meta charset="UTF-8">
	<title>Заметки</title>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>Список заметок (с использованием JSTL)</h1>
	<%--@elvariable id="notes" type="java.util.List"--%>
	<c:forEach var="note" items="${notes}">
		<%--@elvariable id="note" type="by.vsu.Note"--%>
		<c:choose>
			<c:when test="${note.done}">
				<c:set var="cls" value="done"/>
			</c:when>
			<c:otherwise>
				<c:remove var="cls"/>
			</c:otherwise>
		</c:choose>
		<div>
			<p class="${cls}"><strong>${note.title}</strong></p>
			<p class="${cls}">${note.description}</p>
		</div>
	</c:forEach>
	<h2>Итого заметок: ${fn:length(notes)}</h2>
	<form method="post" action="save.html">
		<h2>Новая заметка:</h2>
		<div>
			<label>
				Заголовок:
				<input type="text" name="title">
			</label>
		</div>
		<div>
			<label>
				Описание:
				<input type="text" name="description">
			</label>
		</div>
		<button type="submit">Добавить</button>
	</form>
</body>
</html>
