import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Engine engine1 = new Engine();
		engine1.mark = "Abc";
		Engine engine2 = new Engine();
		engine2.mark = "Xyz";
		Transmission transmission1 = new Transmission();
		transmission1.name = "Pqr";
		transmission1.isAuto = true;
		Transmission transmission2 = new Transmission();
		transmission2.name = "Mnk";
		transmission2.isAuto = false;
		Car car = new Car();
		Scanner sc = new Scanner(System.in);
		System.out.print("Введите название машины: ");
		car.name = sc.nextLine();
		System.out.println("Выберите двигатель:");
		System.out.println("1. " + engine1.mark);
		System.out.println("2. " + engine2.mark);
		System.out.print("Введите номер выбранного двигателя: ");
		int engineNumber = sc.nextInt();
		if(engineNumber == 1) {
			car.engine = engine1;
		} else if(engineNumber == 2) {
			car.engine = engine2;
		} else {
			System.out.println("Такого двигателя нет");
			return;
		}
		System.out.println("Выберите коробку передач:");
		System.out.println("1. " + transmission1.name + " (коробка автомат: " + transmission1.isAuto + ")");
		System.out.println("2. " + transmission2.name + " (коробка автомат: " + transmission2.isAuto + ")");
		System.out.print("Введите номер выбранной коробки передач: ");
		int transmissionNumber = sc.nextInt();
		if(transmissionNumber == 1) {
			car.transmission = transmission1;
		} else if(transmissionNumber == 2) {
			car.transmission = transmission2;
		} else {
			System.out.println("Такой коробки передач нет");
			return;
		}
		System.out.print("Введите массу автомобиля: ");
		car.weight = sc.nextDouble();
		System.out.print("Введите количество пассажиров: ");
		int passengersCount = sc.nextInt();
		car.weight += passengersCount * 80;
		System.out.print("Введите массу багажа: ");
		car.weight += sc.nextDouble();
		System.out.print("Введите мощность работы двигателя: ");
		car.engine.power = sc.nextDouble();
		if(car.transmission.isAuto) {
			if(car.engine.power <= 1000) {
				car.transmission.level = 1;
			} else if(car.engine.power <= 2000) {
				car.transmission.level = 2;
			} else if(car.engine.power <= 5000) {
				car.transmission.level = 3;
			} else {
				car.transmission.level = 4;
			}
		} else {
			System.out.print("Какую передачу включаем? ");
			car.transmission.level = sc.nextInt();
		}
		System.out.println("Машина движется со скоростью " + car.speed() + " км/ч");
	}
}
