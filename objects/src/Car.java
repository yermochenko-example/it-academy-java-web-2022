public class Car {
	String name;
	double weight;
	Engine engine;
	Transmission transmission;
	double speed() {
		return engine.power * transmission.level / 200 * 5000 / weight;
	}
	void stop(boolean handBrake) {
		engine.power = 0;
		transmission.level = 0;
	}
}
