import java.util.Locale;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.ENGLISH);
		System.out.print("Введите целое число: ");
		int x = sc.nextInt();
		System.out.print("Введите дробное число: ");
		double y = sc.nextDouble();
		double z = x + y;
		System.out.println("Результат сложения " + z);
	}
}
