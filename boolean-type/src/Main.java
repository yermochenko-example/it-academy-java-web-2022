public class Main {
	public static void main(String[] args) {
		boolean x = true;
		boolean y = false;
		boolean a, b, c, d, e, f;
		int n = 12;
		int m = 34;
		a = (n > m);
		b = (n < m);
		c = (n >= m);
		d = (n <= m);
		e = (n == m);
		f = (n != m);
		boolean p = true;
		boolean q = false;
//		a = (p > q);  // ERROR
//		b = (p < q);  // ERROR
//		c = (p >= q); // ERROR
//		d = (p <= q); // ERROR
		boolean u, v, w;
		u = !a;     // NOT a
		v = b && c; // b AND c
		w = d || e; // d OR e
	}
}
