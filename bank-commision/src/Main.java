import static java.lang.Math.ceil;

import java.util.Scanner;

public class Main {
	public static String money(int sum) {
		int commissionRub = sum / 100;
		int commissionKop = sum % 100;
		return commissionRub + " руб. " + commissionKop + " коп.";
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Введите тип первого счёта (1 - физическое лицо, 2 - юридическое лицо): ");
		int accountType1 = sc.nextInt();
		System.out.print("Введите тип второго счёта (1 - физическое лицо, 2 - юридическое лицо): ");
		int accountType2 = sc.nextInt();
		System.out.print("Введите сумму (количество рублей и количество копеек, разделённые пробелом): ");
		int sumRub = sc.nextInt();
		int sumKop = sc.nextInt();
		int sum = sumRub * 100 + sumKop;
		int commission;
		if(accountType1 == 1 && accountType2 == 1) {
			double percent;
			if(sum < 1_000_00) {
				percent = 0.09;
			} else {
				percent = 0.11;
			}
			commission = (int) ceil(sum * percent);
		} else if(accountType1 == 1 && accountType2 == 2) {
			commission = 0;
		} else if(accountType1 == 2 && accountType2 == 1) {
			commission = 1_00;
		} else {
			commission = (int) ceil(sum * 0.15);
		}
		System.out.println("Комиссия банку: " + money(commission));
	}
}
