public class Main {
	public static void main(String[] args) {
		// Целые типы данных
		byte   x1; // 1 байт
		short  x2; // 2 байта
		int    x3; // 4 байта
		long   x4; // 8 байт
		// Вещественные типы данных
		float  x5; // 4 байта
		double x6; // 8 байт
	}
}
